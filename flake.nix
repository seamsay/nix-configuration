{
  inputs = {
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-utils-plus.url = "github:gytis-ivaskevicius/flake-utils-plus";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nu-scripts = {
      url = "github:nushell/nu_scripts";
      flake = false;
    };
  };

  outputs = inputs @ {
    self,
    home-manager,
    flake-utils-plus,
    nixpkgs,
    nu-scripts,
  }:
    flake-utils-plus.lib.mkFlake {
      inherit self inputs;

      channelsConfig = {allowUnfree = true;};

      hostDefaults.modules = [
        home-manager.nixosModules.home-manager
        {
          home-manager = {
            extraSpecialArgs = {inherit nu-scripts;};
            useUserPackages = true;
            useGlobalPkgs = true;
            sharedModules = [./home-manager/shared];
          };
        }
        ./nixos/shared.nix
      ];

      hosts = {
        media = {
          system = "x86_64-linux";
          modules = [
            ./nixos/architectures/x86_64.nix
            ./nixos/hosts/starr/shared.nix
            ./nixos/hosts/starr/media.nix
            ./nixos/seat/headless.nix
            ./nixos/platforms/t570.nix
            {
              home-manager.sharedModules = [
                ./home-manager/association/personal.nix
                ./home-manager/platforms/linux.nix
                ./home-manager/seat/headless.nix
              ];
            }
          ];
        };
        starr = {
          system = "aarch64-linux";
          modules = [
            ./nixos/architectures/aarch64.nix
            ./nixos/hosts/starr/shared.nix
            ./nixos/hosts/starr/starr.nix
            ./nixos/platforms/rpi4.nix
            {
              home-manager.sharedModules = [
                ./home-manager/association/personal.nix
                ./home-manager/platforms/linux.nix
                ./home-manager/seat/headless.nix
              ];
            }
          ];
        };
        undecided = {
          system = "aarch64-linux";
          modules = [
            ./nixos/architectures/aarch64.nix
            ./nixos/hosts/undecided.nix
            ./nixos/platforms/rpi4.nix
            {
              home-manager.sharedModules = [
                ./home-manager/association/personal.nix
                ./home-manager/platforms/linux.nix
                ./home-manager/seat/headless.nix
              ];
            }
          ];
        };
      };

      homeConfigurations.srm-mv-MBP = home-manager.lib.homeManagerConfiguration {
        pkgs = nixpkgs.legacyPackages.aarch64-darwin;
        modules = [
          {
            home = {
              username = "sean";
              homeDirectory = "/Users/sean";
            };
            programs.home-manager.enable = true;

            # Enable signing by default here because this is currently the only machine with my private key.
            programs.git.signing.signByDefault = true;
          }
          ./home-manager/shared
          ./home-manager/association/qub.nix
          ./home-manager/development
          ./home-manager/platforms/darwin.nix
        ];
        extraSpecialArgs = {inherit nu-scripts;};
      };
    }
    // (flake-utils-plus.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {inherit system;};

      # `alejandra` now defaults to stdin instead of the current directory.
      alejandra = pkgs.writeShellApplication {
        name = "alejandra";
        text = ''
          ${pkgs.alejandra}/bin/alejandra .
        '';
      };
    in {formatter = alejandra;}));
}
