{
  programs.git.userEmail = "smarshallsay01@qub.ac.uk";

  programs.ssh.matchBlocks = {
    aigis-offcampus = {
      hostname = "aigis-login.mp.qub.ac.uk";
      identityFile = "~/.ssh/id_ed25519";
      user = "smarshallsay01";
      proxyJump = "bifrost";
    };
    aigis-oncampus = {
      host = "aigis-login.mp.qub.ac.uk aigis-oncampus aigis";
      hostname = "aigis-login.mp.qub.ac.uk";
      identityFile = "~/.ssh/id_ed25519";
      user = "smarshallsay01";
    };
    archer2 = {
      host = "login.archer2.ac.uk archer2";
      hostname = "login.archer2.ac.uk";
      identityFile = "~/.ssh/id_ed25519-ARCHER2";
    };
    archer2-x11 = {
      host = "archer2-x11";
      hostname = "login.archer2.ac.uk";
      identityFile = "~/.ssh/id_ed25519-ARCHER2";
      forwardX11 = true;
      forwardX11Trusted = true;
    };
    bifrost = {
      hostname = "bifrost.mp.qub.ac.uk";
      user = "smarshallsay01";
      identityFile = "~/.ssh/id_rsa.bifrost.txt";
    };
    kelvin2-offcampus = {
      hostname = "login.kelvin.alces.network";
      user = "40382543";
      port = 55890;
      # Kelvin2 uses a bunch of different login nodes and so you need some lines in `known_hosts` to not change.
      extraOptions = {
        UpdateHostKeys = "no";
      };
    };
    kelvin2-oncampus = {
      host = "kelvin2.qub.ac.uk kelvin2-oncampus kelvin2";
      hostname = "kelvin2.qub.ac.uk";
      user = "40382543";
      # Kelvin2 uses a bunch of different login nodes and so you need some lines in `known_hosts` to not change.
      extraOptions = {
        UpdateHostKeys = "no";
      };
    };
  };
}
