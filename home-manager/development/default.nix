{pkgs, ...}: {
  # TODO: Set up vscode to be statically configured.

  home.sessionVariables.RUST_SRC_PATH = pkgs.rustPlatform.rustLibSrc;
  home.packages = with pkgs; [hyperfine niv rust-analyzer];

  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
    enableBashIntegration = false;
    enableNushellIntegration = false;
    enableZshIntegration = false;
  };

  programs.git.lfs.enable = true;

  imports = [./julia.nix];
}
