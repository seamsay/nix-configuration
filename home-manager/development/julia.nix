{pkgs, ...}: let
  # Source build currently broken on Mac, and too resource intensive on the RPis.
  julia =
    if pkgs.stdenv.isDarwin || pkgs.stdenv.isAarch64
    then pkgs.julia-bin
    else pkgs.julia;
in {
  home = {
    packages = [julia];

    # TODO: CFF Citation.
    # TODO: Include README in website.
    file.".julia/config/startup.jl".text = ''
      function pkg_template()
          @eval begin
              using PkgTemplates
              Template(;
                  user = "seamsay",
                  dir = "",
                  host = "gitlab.com",
                  julia = v"1.2",
                  plugins = [
                      ProjectFile(),
                      SrcDir(),
                      Tests(;
                          project = true,
                          aqua = true,
                          jet = true,
                      ),
                      Readme(),
                      License(),
                      Git(;
                          ssh = true,
                      ),
                      !GitHubActions,
                      !CompatHelper,
                      !TagBot,
                      GitLabCI(;
                          extra_versions=["1.2", "1.9"],
                      ),
                      Documenter{GitLabCI}(),
                      Formatter(),
                  ],
              )
          end
      end
    '';
  };
}
