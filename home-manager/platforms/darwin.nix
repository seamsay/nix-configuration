{
  lib,
  pkgs,
  ...
}: {
  nix.package = pkgs.nix;

  home.packages = with pkgs; [pueue];

  launchd.agents.pueued = {
    enable = true;
    config = {
      ProgramArguments = ["${pkgs.pueue}/bin/pueued" "-v"];
      KeepAlive = {
        Crashed = true;
        SuccessfulExit = false;
      };
      ProcessType = "Background";
      RunAtLoad = true;
    };
  };

  programs.nushell.envFile.text = ''
    do --env {
        let index = ($env.PATH | split row (char esep) | enumerate | find --regex "\\bnix\\b" | last | get index) + 1
        for bin in (ls /Users/sean/Library/Python/ | each { [$in.name "bin"] | str join '/' } | sort) {
            $env.PATH = ($env.PATH | split row (char esep) | insert $index $bin)
        }
    }
  '';

  programs.ssh.extraConfig = "UseKeychain yes";
}
