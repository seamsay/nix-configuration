{pkgs, ...}: {
  home.packages = with pkgs; [udiskie];

  services.pueue.enable = true;
}
