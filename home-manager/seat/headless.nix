{pkgs, ...}: {
  # Home Manager's udiskie module expects you to be running a GUI, but we won't be.
  systemd.user.services.udiskie = {
    Unit.Description = "udiskie Mount Daemon";
    Service.ExecStart = "${pkgs.udiskie}/bin/udiskie";
    Install.WantedBy = ["multi-user.target"];
  };
}
