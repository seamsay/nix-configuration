{
  lib,
  pkgs,
  ...
}: {
  home = {
    enableNixpkgsReleaseCheck = true;
    stateVersion = "24.05";
  };

  home = {
    sessionVariables.EDITOR = "${pkgs.kakoune}/bin/kak";
    packages = with pkgs; [
      bottom
      file
      git
      kakoune
    ];
  };

  home.sessionVariables.PYTHONSTARTUP = pkgs.writeText ".pythonrc" ''
    def import_file(path, name=None):
        import import_lib.utils
        import sys
        import os.path

        if name is None:
            name = os.path.splitext(os.path.basename(path))[0]

        spec = import_lib.utils.spec_from_file_location(name, path)
        module = import_lib.module_from_spec(spec)
        sys.modules[name] = module
        spec.loader.exec_module(module)
  '';

  nix = {
    gc.automatic = true;
    settings = {
      auto-optimise-store = true;
      experimental-features = ["flakes" "nix-command" "no-url-literals"];
      substituters = [
        "https://nix-community.cachix.org"
        "https://cache.nixos.org"
      ];
    };
  };

  nixpkgs.config = import ./nixpkgs-config.nix;
  xdg.configFile."nixpkgs/config.nix".source = ./nixpkgs-config.nix;

  programs = {
    eza = {
      enable = true;
      icons = "auto";
      git = true;
    };
    fd.enable = true;
    ripgrep.enable = true;
  };

  programs.matplotlib = {
    enable = true;
    config = {
      axes.grid = true;
      grid.alpha = 0.33;
    };
  };

  # TODO: Maybe keychain would be a better solution than forwardAgent?
  programs.ssh = {
    enable = true;

    addKeysToAgent = "yes";

    extraConfig = ''
      SendEnv COLORTERM LC_TERMINAL TERM_PROGRAM
    '';

    matchBlocks = let
      home-ip = "86.155.160.59";

      localIPs = {
        media = "192.168.1.238";
        starr = "192.168.1.68";
        undecided = "192.168.1.93";
      };

      remoteIPs = {
        media = 8238;
      };

      localMatch = host: ip: {
        host = "${ip} ${host} ${host}.local";
        hostname = ip;
        user = "sean";
        forwardAgent = true;
      };

      localMatches = lib.concatMapAttrs (host: ip: {"${host}.local" = localMatch host ip;}) localIPs;

      remoteMatch = port: {
        inherit port;
        hostname = home-ip;
        user = "sean";
        forwardAgent = true;
      };

      remoteMatches = lib.concatMapAttrs (host: port: {"${host}.remote" = remoteMatch port;}) remoteIPs;
    in
      localMatches // remoteMatches;
  };

  imports = [./git.nix ./gpg ./nushell];
}
