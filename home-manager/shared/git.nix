{pkgs, ...}: {
  programs.git = {
    enable = true;

    userName = "Sean Marshallsay";

    aliases.fixup = let
      fixup = pkgs.writeShellApplication {
        name = "git-fixup";
        text = ''
          if ! git diff-files --quiet
          then
              1>&2 echo 'ERROR: Working directory has unstaged changes.'
              # shellcheck disable=SC2016
              1>&2 echo 'NOTE: Please commit, or stash with `--keep-index`.'
              exit 3
          fi

          commit="$(git rev-parse "$1")"
          git commit --no-verify --fixup="$commit"
          git rebase -i --autosquash "$commit"~1
        '';
      };
    in "! ${fixup}/bin/git-fixup";

    difftastic = {
      enable = true;
      background = "light";
    };

    extraConfig = {
      commit.verbose = true;
      diff = {
        ignoreSubmodules = "all";
        mnemonicPrefix = true;
      };
      github.user = "Sean1708";
      gitlab.user = "seamsay";
      init.defaultBranch = "main";
      merge.ff = false;
      pull.ff = "only";
      push.default = "simple";
      rebase.autosquash = true;
      rerere.enabled = true;
    };

    ignores = [
      "/.direnv/"
      "/.envrc"
      "/.mypy_cache/"
      "__pycache__/"
      "/.vscode/"
      ".DS_Store"
    ];

    signing.key = null;
  };
}
