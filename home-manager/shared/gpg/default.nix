{
  programs.gpg = {
    enable = true;
    mutableKeys = false;
    mutableTrust = false;

    publicKeys = [
      {
        source = ./SeanMarshallsay-Personal-3D3F2E3CADA67C24.asc;
        trust = "ultimate";
      }
    ];
  };
}
