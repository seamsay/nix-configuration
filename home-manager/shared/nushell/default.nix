{
  nu-scripts,
  pkgs,
  ...
}: let
  shell =
    if pkgs.stdenv.isDarwin
    then "zsh"
    else "bash";
in {
  programs.${shell} = {
    enable = true;
    initExtra = ''
      # HACK: Occasionally we want to override nix versions of software with our own, e.g. on macOS we want to use the system SSH for `UseKeychain` support.
      #       We can do this by symlinking the software in this directory which then gets added to the top of the PATH.
      export PATH="$HOME/.local/nix-overrides:$PATH"

      # NOTE: When connecting remotely with SSH one can use `ssh -o 'SetEnv NO_NU=1' ...` to avoid starting `nu`.
      if [ -z "$NO_NU" ] && [ -x "$(command -v nu)" ]
      then
        # VS Code runs the shell as interactive when it tries to resolve the shell environment.
        # The two electron environment variables seem to only be set when VS Code is running that resolver.
        if [ "$VSCODE_RESOLVING_ENVIRONMENT" != 1 ] && [ "$ELECTRON_RUN_AS_NODE" != 1 ] && [ "$ELECTRON_NO_ATTACH_CONSOLE" != 1 ]
        then
          exec nu
        fi
      fi
    '';
  };

  programs.nushell = {
    enable = true;

    configFile.source = ./config.nu;
    extraConfig = ''
      use ${nu-scripts}/modules/background_task/task.nu
    '';

    envFile.text = ''
      let path_conversions = {
        from_string: { |s| $s | split row (char esep) | path expand --no-symlink },
        to_string: { |v| $v | path expand --no-symlink | str join (char esep) },
      }

      $env.ENV_CONVERSIONS = {
        "PATH": $path_conversions,
        "PYTHONPATH": $path_conversions,
      }
    '';
  };

  programs.starship = {
    enable = true;
    enableNushellIntegration = true;
    settings = {
      right_format = "$time";

      directory.fish_style_pwd_dir_length = 1;
      git_metrics.disabled = false;
      # TODO: Keep an eye on https://github.com/NixOS/nix/issues/6677
      nix_shell.heuristic = true;
      shlvl.disabled = false;
      status.disabled = false;
      time = {
        disabled = false;
        format = "[$time]($style)";
      };
    };
  };
}
