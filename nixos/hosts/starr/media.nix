{pkgs, ...}: {
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/5787a6ef-1b90-4266-8226-f662b2a4ed12";
      fsType = "ext4";
    };

    "/boot" = {
      device = "/dev/disk/by-uuid/1FCB-71DA";
      fsType = "vfat";
    };

    "/media/Media" = {
      device = "/dev/disk/by-uuid/97e0b16c-9afd-4992-96c6-38d926ae3e4a";
      fsType = "ext4";
    };
  };

  swapDevices = [
    {device = "/dev/disk/by-uuid/416413fb-98fc-442c-9363-116e9641b756";}
  ];

  powerManagement.cpuFreqGovernor = "ondemand";

  networking = {
    hostName = "media";
    firewall = let
      nfsPort = 2049;
    in {
      allowedTCPPorts = [nfsPort];
      allowedUDPPorts = [nfsPort];
    };
  };

  # Transmission peer ports don't work when IPv6 is enabled.
  # NOTE: You'll need to enable and then disable this again after a reboot.
  networking.enableIPv6 = false;

  fileSystems."/nfs/Media" = {
    device = "/media/Media";
    options = ["bind"];
  };

  services.nfs.server = {
    enable = true;
    # NOTE: The IP address is of the *client*!!!
    exports = ''
      /nfs        192.168.1.68(rw,fsid=0,no_subtree_check)
      /nfs/Media  192.168.1.68(rw,sync,insecure,nohide,no_subtree_check)
    '';
  };

  users.groups.starr.members = [
    "jellyfin"
    "sean"
    "transmission"
  ];

  services.jellyfin = {
    enable = true;
    openFirewall = true;
  };

  services.transmission = {
    enable = true;
    openRPCPort = true;
    settings = {
      download-dir = "/media/Media/Torrents/Complete";
      incomplete-dir = "/media/Media/Torrents/Incomplete";
      port-forwarding-enabled = false;
      rpc-bind-address = "0.0.0.0";
      rpc-whitelist = "127.0.0.*,192.168.*.*";
    };
  };

  systemd.timers."transmission-port-forwarding" = {
    wantedBy = ["timers.target"];
    timerConfig = {
      OnBootSec = "45s";
      OnUnitActiveSec = "45s";
      Unit = "transmission-port-forwarding.service";
    };
  };

  systemd.services."transmission-port-forwarding" = {
    serviceConfig = {
      Type = "oneshot";
      User = "root";
    };
    script = ''
      set -u

      renew_port() {
        protocol="$1"
        port_file="$HOME/.local/state/transmission-$protocol-port"

        result="$(${pkgs.libnatpmp}/bin/natpmpc -a 1 0 "$protocol" 60 -g 10.2.0.1)"
        echo "$result"

        new_port="$(echo "$result" | ${pkgs.ripgrep}/bin/rg --only-matching --replace '$1' 'Mapped public port (\d+) protocol ... to local port 0 lifetime 60')"
        old_port="$(cat "$port_file" || echo '-1')"
        echo "Mapped new $protocol port $new_port, old one was $old_port."
        echo "$new_port" >"$port_file"

        if ${pkgs.iptables}/bin/iptables -C INPUT -p "$protocol" --dport "$new_port" -j ACCEPT
        then
          echo "New $protocol port $new_port already open, not opening again."
        else
          echo "Opening new $protocol port $new_port."
          ${pkgs.iptables}/bin/iptables -I INPUT -p "$protocol" --dport "$new_port" -j ACCEPT
        fi

        if [ "$protocol" = tcp ]
        then
          echo "Telling transmission to listen on peer port $new_port."
          ${pkgs.transmission}/bin/transmission-remote --port "$new_port"
        fi

        if [ "$new_port" -eq "$old_port" ]
        then
          echo "New $protocol port $new_port is the same as old port $old_port, not closing old port."
        else
          if ${pkgs.iptables}/bin/iptables -C INPUT -p "$protocol" --dport "$old_port" -j ACCEPT
          then
            echo "Closing old $protocol port $old_port."
            ${pkgs.iptables}/bin/iptables -D INPUT -p "$protocol" --dport "$old_port" -j ACCEPT
          else
            echo "Old $protocol port $old_port not open, not attempting to close."
          fi
        fi
      }

      renew_port udp
      renew_port tcp
    '';
  };


  # Remote building for Raspberry-Pis.

  # TODO: This user should have no home directory (see https://nix.dev/tutorials/nixos/distributed-builds-setup.html#set-up-the-remote-builder).
  users.users.remote-builder = {
    uid = 1002;
    isNormalUser = true;
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAx0UNuXimx3xn9lA7S458uF97GOkRxSx6+iCPBdYpL7 root@starr"
    ];
  };

  nix.settings.trusted-users = ["remote-builder"];

  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];

  nixpkgs.config.crossSystem.config = "aarch64-linux";

  systemd.services.nix-daemon.serviceConfig = {
    MemoryAccounting = true;
    MemoryMax = "75%";
    OOMScoreAdjust = 500;
  };
}
