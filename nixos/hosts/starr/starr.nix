{lib, pkgs, ...}: {
  nixpkgs.config.permittedInsecurePackages = [
    "aspnetcore-runtime-6.0.36"
    "aspnetcore-runtime-wrapped-6.0.36"
    "dotnet-sdk-6.0.428"
    "dotnet-sdk-wrapped-6.0.428"
  ];

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-uuid/44444444-4444-4444-8888-888888888888";
      fsType = "ext4";
    };
    "/media/Media" = {
      device = "192.168.1.238:/Media";
      fsType = "nfs";
      options = ["x-systemd.automount" "noauto"];
    };
  };

  networking = {
    hostName = "starr";
    firewall.allowedTCPPorts = [
      8191 # FlareSolverr
    ];
  };

  users.groups = {
    docker.members = ["sean"];
    starr.members = [
      "bazarr"
      "prowlarr"
      "radarr"
      "sean"
      "sonarr"
    ];
  };

  virtualisation.oci-containers = {
    backend = "docker";
    containers.flaresolverr = {
      image = "flaresolverr/flaresolverr";
      ports = ["8191:8191"];
      environment = {
        LOG_LEVEL = "info";
      };
    };
  };

  services.bazarr = {
    enable = true;
    openFirewall = true;
  };

  services.prowlarr = {
    enable = true;
    openFirewall = true;
  };

  services.radarr = {
    enable = true;
    openFirewall = true;
  };

  services.sonarr = {
    enable = true;
    package = pkgs.sonarr.overrideAttrs (lib.const { doCheck = false; });
    openFirewall = true;
  };
}
