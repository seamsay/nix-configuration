{modulesPath, ...}: {
  imports = [
    "${modulesPath}/installer/scan/not-detected.nix"
  ];

  boot.initrd.availableKernelModules = ["xhci_pci" "usbhid"];

  nix = {
    distributedBuilds = true;
    settings.builders-use-substitutes = true;

    buildMachines = [
      {
        hostName = "media";
        sshUser = "remote-builder";
        sshKey = "/root/.ssh/id_ed25519_remote-builder";
        systems = ["x86_64-linux" "aarch64-linux"];
        supportedFeatures = [ "nixos-test" "big-parrallel" "kvm" ];
      }
    ];
  };

  nixpkgs.hostPlatform = "aarch64-linux";

  powerManagement.cpuFreqGovernor = "ondemand";

  swapDevices = [
    {
      device = "/swapfile";
      size = 1024;
    }
  ];
}
