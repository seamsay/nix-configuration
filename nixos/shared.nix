{
  config,
  pkgs,
  ...
}: {
  # Before changing this value read the documentation for this option (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.05";

  home-manager = {
    users = {
      root = {};
      sean = {};
    };
  };

  nix = {
    optimise.automatic = true;
    settings = {
      trusted-public-keys = [
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
        "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
      ];
      trusted-substituters = [
        "https://nix-community.cachix.org"
        "https://cache.nixos.org"
      ];
      trusted-users = ["root" "@wheel"];
    };
  };

  networking = {
    hosts."81.154.192.109" = ["home"];
    networkmanager = {
      enable = true;
      insertNameservers = [
        # Cloudflare
        "1.1.1.1"
        "1.0.0.1"
        # Control D
        "76.76.2.0"
        "76.76.10.0"
      ];
    };
  };

  time.timeZone = "Europe/London";

  i18n.defaultLocale = "en_GB.UTF-8";
  console.keyMap = "uk";

  users.users.sean = {
    uid = 1001;
    isNormalUser = true;
    extraGroups = ["networkmanager" "wheel"];
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBkrkL0n4UZVNsI8RRuGyDcQKnpQsC4IZeJnV2D4j3lk sean@Seans-MacBook-Pro.local"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIPaVH4ZllJBeQVB/yH/Eog1GtqT3iaVBnHpP5PtcJx3 sean@starr"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBeoA5lJNRpEgKpdiPB+hvnxKzl8mh3lsil3zE98+dZN sean@vpn"
    ];
  };

  environment.systemPackages = with pkgs; [
    nano
    python3
    vim
  ];

  programs.ssh.startAgent = true;

  services.openssh = {
    enable = true;
    settings = {
      AcceptEnv = "COLORTERM LC_TERMINAL NO_NU TERM_PROGRAM";
      PermitRootLogin = "no";
    };
  };

  services.udisks2.enable = true;

  security.sudo = {
    extraConfig = ''Defaults env_keep += "COLORTERM LC_TERMINAL SSH_CLIENT SSH_CONNECTION SSH_TTY TERM_PROGRAM"'';
    execWheelOnly = true;
    wheelNeedsPassword = false;
  };
}
